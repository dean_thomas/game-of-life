const LOG_LIMIT = 16;
const FPS = 10;
const DEAD = 0;
const ALIVE = 1;

var logLine = 0;
var frame = 0;
var stopped = true;
var generation = 1;
var cell_width, cell_height;
var cells = [[]];
var world_width = 20;
var world_height = 20;

var canvas = document.getElementById("canvas1");
// attaching the sketchProc function to the canvas
var p = new Processing(canvas, sketchProc);
// p.exit(); to detach it

function createCellArray(width, height) {
    //  Creates a 2D array of cell states
    var result = new Array(width);
    for (var x = 0; x < width; ++x) {
        result[x] = new Array(height);
        for (var y = 0; y < height; ++y) {
            result[x][y] = DEAD;
        }
    }
    return result;
}

function resetWorld() {
    log("Clearing cells");
    cells = createCellArray(world_width, world_height);
}

function heightChange(value) {
    log("Height set to: " + value);
    world_height = value;
    resetWorld();
}

function widthChange(value) {
    log("Width set to: " + value);
    world_width = value;
    resetWorld();
}

function initialiseComboBoxes() {
    //  Fills the height and width combox boxes with values from [1..30].
    for (var i = 1; i < 31; ++i) {
        document.getElementById("cmbWidth").innerHTML += "<option value='" + i + "'>" + i + "</option>";
        document.getElementById("cmbHeight").innerHTML += "<option value='" + i + "'>" + i + "</option>";
    }

    document.getElementById("cmbWidth").innerHTML += "<option value='" + 50 + "'>" + 50 + "</option>";
    document.getElementById("cmbHeight").innerHTML += "<option value='" + 50 + "'>" + 50 + "</option>";
    document.getElementById("cmbWidth").innerHTML += "<option value='" + 100 + "'>" + 100 + "</option>";
    document.getElementById("cmbHeight").innerHTML += "<option value='" + 100 + "'>" + 100 + "</option>";
    //document.getElementById("cmbWidth").innerHTML += "<option value='" + 250 + "'>" + 250 + "</option>";
    //document.getElementById("cmbHeight").innerHTML += "<option value='" + 250 + "'>" + 250 + "</option>";
    //document.getElementById("cmbWidth").innerHTML += "<option value='" + 500 + "'>" + 500 + "</option>";
    //document.getElementById("cmbHeight").innerHTML += "<option value='" + 500 + "'>" + 500 + "</option>";
    //document.getElementById("cmbWidth").innerHTML += "<option value='" + 1000 + "'>" + 1000 + "</option>";
    //document.getElementById("cmbHeight").innerHTML += "<option value='" + 1000 + "'>" + 1000 + "</option>";
}

function flipCell(x, y) {
    log("Flipping cell (" + x + ", " + y + ")");

    //  Flips a specific cell ALIVE / DEAD
    if (cells[x][y] === DEAD) 
        cells[x][y] = ALIVE;
    else 
        cells[x][y] = DEAD;
}

function canvasClicked(event) {
    translateMouseToCell = function () {
        //  Translates the mouse click position to a cell index.
        translateMouseToCanvas = function () {
            //  Translates the mouse click to the canavs local coordinates.
            var rect = canvas.getBoundingClientRect();
            return {
                x: Math.floor((event.clientX - rect.left) / (rect.right - rect.left) * canvas.width),
                y: Math.floor((event.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height)
            }
        };

        var local = translateMouseToCanvas();
        return {
            //  Round down to pick the correct cell
            x: Math.floor(local.x / cell_width),
            y: Math.floor(local.y / cell_height)
        }
    };

    //  Do the translation to a cell
    var clickedCell = translateMouseToCell();
    log("Mouse clicked at: (" + event.x + ", " + event.y + ") ", false);
    log("Cell: (" + clickedCell.x + ", " + clickedCell.y + ")", true);

    //  Flip the state of the cell
    flipCell(clickedCell.x, clickedCell.y);
}

function startStop() {
    stopped = !stopped;
    var text = stopped ? "start" : "stop";
    document.getElementById("btnStop").setAttribute("value", text);
}

function randomize() {
    for (var x = 0; x < world_width; ++x) {
        for (var y = 0; y < world_height; ++y) {
            cells[x][y] = Math.round(Math.random());
        }
    }
}

///	JavaScript doesn't have a 'proper' modulus function
function mod(x, y) {
    return x - y * Math.floor(x / y);
}

function log(text, newLine = true) {
    //  Clear the log every LOG_LIMIT lines
    if (mod(logLine, LOG_LIMIT) == 0) document.getElementById("log").innerHTML = "";

    //  Update the counter and log the text
    document.getElementById("log").innerHTML += text + (newLine ? "<br>" : "");
    if (newLine)++logLine;
}

function mooreNeighbourhood(x, y) {
    var count = 0;
    for (var dx = -1; dx < 2; ++dx) {
        for (var dy = -1; dy < 2; ++dy) {
            if ((dx !== 0) || (dy !== 0)) {
                //var x0 = mod((x+dx), world_width);				
                var x0 = mod((x + dx), world_width);
                var y0 = mod((y + dy), world_height);
                //log("" + x0 + ", " + y0);
                //log("" + cells[x0][y0], false);
                if (cells[x0][y0] === ALIVE)
                    ++count;
            } else {
                //log("x", false);
            }
        }
        //log("");
    }
    return count;
}

function updateCell(func, x, y) {
    var neighbours = func(x, y);

    if (cells[x][y] == ALIVE) {
        switch (neighbours) {
            case 0:
            case 1:
                //  Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
                return DEAD;
                break;
            case 2:
            case 3:
                return ALIVE;
                //  Any live cell with two or three live neighbours lives on to the next generation.
                break;
            default:
                //  Any live cell with more than three live neighbours dies, as if by overpopulation.
                return DEAD;
        }
    }
    else {
        //  Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
        if (neighbours == 3) return ALIVE;
    }
}

function update() {
    cells = newGeneration(mooreNeighbourhood);
}

function newGeneration(func) {
    var result = createCellArray(world_width, world_height);

    ++generation;
    log("Generation: " + generation);
    for (var x = 0; x < world_width; ++x) {
        for (var y = 0; y < world_height; ++y) {
            result[x][y] = updateCell(func, x, y);
        }
    }
    return result;
}

///
///
///
function sketchProc(processing) {
    processing.setup = function () {
        initialiseComboBoxes();
        processing.size(document.getElementById("canvas1").clientWidth, document.getElementById("canvas1").clientHeight);
        
        cells = createCellArray(world_width, world_height);
        log("Canvas size: " + processing.width + "x" + processing.height)
        log("Cell dims: " + cell_width + "x" + cell_height);
    };

    // Override draw function, by default it will be called 60 times per second
    processing.draw = function () {
        cell_width = processing.width / world_width;
        cell_height = processing.height / world_height;

        if (!stopped) {
            ++frame;
            if (frame >= (60 / FPS)) {
                frame = 0;
                cells = newGeneration(mooreNeighbourhood);
            }
        }

        function drawCell(x, y) {
            if (cells[x][y] == 1) {
                processing.fill(0, 0, 0);
            } else {
                processing.fill(255, 255, 255);
            }
            processing.rect(x * cell_width, y * cell_height, cell_width - 1, cell_height - 1);
        }

        // erase background
        processing.background(224);

        for (var x = 0; x < world_width; ++x) {
            for (var y = 0; y < world_height; ++y) {
                drawCell(x, y);
            }
        }
    };
}

